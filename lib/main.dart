import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Practice 5',
      theme: ThemeData.dark(),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final customPaint = CustomPaint(
    painter: MyPainter(),
    child: Container(),
  );
  bool sort = true;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: [
            Container(
              height: 700.0,
              padding: EdgeInsets.fromLTRB(10.0, 38.0, 10.0, 10.0),
              child: Stack(
                children: [
                  Center(
                    child: CustomPaint(
                      size: Size(200.0, 200.0),
                      painter: MyPainter(),
                    ),
                  ),

                  ListView.builder(
                    reverse: sort,
                    itemCount: 101,
                    itemBuilder: (_, index) {
                      return Container(
                        height: 100.0,
                        decoration: BoxDecoration(
                          color: Colors.white12,
                          border: Border.all(width: 4.0),
                        ),
                        child: Center(
                          child: Text(
                            '$index',
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  sort = !sort;
                });
              },
              child: Text('Sort'),
            ),
          ],
        ),
      ),
    );
  }
}



class MyPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawCircle(
      Offset(100.0, 100.0),
     size.width * 0.25,
      Paint()..color = Colors.yellow,
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
